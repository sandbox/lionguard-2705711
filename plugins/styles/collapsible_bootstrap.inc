<?php

/**
 * @file
 * Definition of the collapsible panel style.
 */

$plugin = array(
  'title' => t('Bootstrap collapsible'),
  'description' => t('Allows the panel pane to be collapsed.'),
  'render region' => 'panels_bootstrap_compatible_render_region',
  'render pane' => 'panels_bootstrap_compatible_render_pane',
  'settings form' => 'panels_bootstrap_compatible_region_settings_form',
  'pane settings form' => 'panels_bootstrap_compatible_pane_settings_form',
);

/**
 * Bootstrap panels classes.
 */

function _panels_bootstrap_compatible_paneltypes(){
	return array(
		'panel-default'=>'panel-default',
		'panel-primary'=>'panel-primary',
		'panel-success'=>'panel-success',
		'panel-info'=>'panel-info',
		'panel-warning'=>'panel-warning',
		'panel-danger'=>'panel-danger'
	); 
}
 
/**
 * Settings form callback for region settings.
 */
function panels_bootstrap_compatible_region_settings_form($style_settings) {
  $form = array();
	$paneltypes = _panels_bootstrap_compatible_paneltypes();
  $form['region_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title used as clickable text to open / close collapsed region.'),
    '#required' => TRUE,
    '#default_value' => isset($style_settings['region_title']) ? $style_settings['region_title'] : '',
  );
  $form['region_panelclass'] = array(
    '#type' => 'select',
    '#title' => t('Bootstrap panel class'),
		'#options' => $paneltypes,
    '#default_value' => isset($style_settings['region_panelclass']) ? $style_settings['region_panelclass'] : $paneltypes[0],
  );	
  $form['region_collapsed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapsed by default'),
    '#default_value' => isset($style_settings['region_collapsed']) ? $style_settings['region_collapsed'] : FALSE,
  );
  return $form;
}

/**
 * Settings form callback for pane settings.
 */
function panels_bootstrap_compatible_pane_settings_form($style_settings) {
  $form = array();
	$paneltypes = _panels_bootstrap_compatible_paneltypes();
  $form['pane_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('Title used as clickable text to open / close collapsed region. Use %title for the original pane title.'),
    '#required' => TRUE,
    '#default_value' => isset($style_settings['pane_title']) ? $style_settings['pane_title'] : '%title',
  );
  $form['pane_panelclass'] = array(
    '#type' => 'select',
    '#title' => t('Bootstrap panel class'),
		'#options' => $paneltypes,
    '#default_value' => isset($style_settings['pane_panelclass']) ? $style_settings['pane_panelclass'] : $paneltypes[0],
  );		
  $form['pane_collapsed'] = array(
    '#type' => 'checkbox',
    '#title' => t('Collapsed by default'),
    '#default_value' => isset($style_settings['pane_collapsed']) ? $style_settings['pane_collapsed'] : FALSE,
  );

  return $form;
}

/**
 * Render callback for regions.
 *
 * @ingroup themeable
 */
function theme_panels_bootstrap_compatible_render_region($vars) {
  $settings = $vars['settings'];
  $content = implode('<div class="panel-separator"></div>', $vars['panes']);

  if (empty($settings['region_title'])) {
    $settings['region_title'] = '<empty>';
  }
  if (empty($settings['region_collapsed'])) {
    $settings['region_collapsed'] = FALSE;
  }
	
  $title = ctools_context_keyword_substitute($settings['region_title'], array(), $vars['display']->context);

  return theme('panels_bootstrap_compatible', array(
    'handle' => $title,
    'content' => $content,
    'collapsed' => $settings['region_collapsed'],
		'region_id' => $vars['region_id'],
		'region_panelclass' => $settings['region_panelclass'],
  ));
}

/**
 * Render callback for panes.
 *
 * @ingroup themeable
 */
function theme_panels_bootstrap_compatible_render_pane($vars) {
  $content = $vars['content'];
  $pane = $vars['pane'];
  $display = $vars['display'];
  $settings = $vars['settings'];

  if (empty($content->content)) {
    return;
  }

  if (!empty($settings['pane_title'])) {

    if (empty($settings['pane_collapsed'])) {
      $settings['pane_collapsed'] = FALSE;
    }
    if (is_array($content->content)) {
      $content->content = drupal_render($content->content);
    }
    $pane_title = !empty($content->title) ? $content->title : '';
    $substitutions = array('%title' => $pane_title);
    $title = ctools_context_keyword_substitute($settings['pane_title'], $substitutions, $vars['display']->context);

    return theme('panels_bootstrap_compatible', array(
      'handle' => $title,
      'content' => $content->content,
      'collapsed' => $settings['pane_collapsed'],
      'pane' => $pane,
			'pane_panelclass' => $settings['pane_panelclass'],
    ));
  }

  return theme('panels_pane', array(
    'content' => $content,
    'pane' => $pane,
    'display' => $display,
  ));
}

/**
 * Render a collapsible div.
 *
 * @ingroup themeable
 */
function theme_panels_bootstrap_compatible($vars) {
  $pane_css = $pane_id = $div_id ='';
  if (isset($vars['pane']) && is_object($vars['pane'])) {
    $pane_css = $vars['pane']->css;
    $pane_id = !empty($pane_css['css_id']) ? ' id="' . $pane_css['css_id'] . '"' : '';
    $pane_css = !empty($pane_css['css_class']) ? ' ' . $pane_css['css_class'].' '.$vars['pane_panelclass'] : ' '.$vars['pane_panelclass'];
		$div_id = $vars['pane']->panel.'_'.$vars['pane']->pid;
  }
  if (isset($vars['region_id'])) {
    $div_id = 'block_'.$vars['region_id'];
		$pane_css .= $vars['region_panelclass'];
  }	
	$output = "";
  $class = $vars['collapsed'] ? '' : 'in';
	$output .= '<div '.$pane_id.' class="panel '.$pane_css.'">';
  $output .= '<div class="panel-heading" data-toggle="collapse" data-target="#'.$div_id.'"><h2>' . $vars['handle'] . '</h2></div>';
  $output .= '<div class="panel-body collapse '.$class.'" id="'.$div_id.'">' . $vars['content'] . '</div>';
  $output .= '</div>';

  return $output; 
}
